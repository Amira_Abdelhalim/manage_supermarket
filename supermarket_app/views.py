from django.shortcuts import render
from .models import Customer, Product, Invoice
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic import View
from .forms import InvoiceForm
from django.core.mail import send_mail
from django.contrib import messages
from django.template.loader import render_to_string
from .utils import render_to_pdf
from django.utils.html import strip_tags
from django.contrib.auth.decorators import login_required


# Create your views here
def Home(request):
    return render(request, 'customer.html')


@login_required(login_url='/login/')
def CustomerList(request):
    customers = Customer.objects.all()
    context = {
        'customers': customers
    }
    return render(request, 'customer.html', context)


@login_required(login_url='/login/')
def ViewCustomer(request, id):
    customer = Customer.objects.get(id=id)
    invoices = Invoice.objects.filter(customer_id=id)
    context = {
        'customer': customer,
        'invoices': invoices,
    }
    return render(request, 'view_customer.html', context)


@login_required(login_url='/login/')
def ProductList(request):
    products = Product.objects.all()
    context = {
        'products': products
    }
    return render(request, 'product.html', context)


@login_required(login_url='/login/')
def InvoiceList(request):
    invoices = Invoice.objects.all()
    context = {
        'invoices': invoices,
    }
    return render(request, 'invoice.html', context)



@login_required(login_url='/login/')
def CreateInvoice(request):
    if request.method == 'POST':
        invoice_form = InvoiceForm(request.POST)
        if invoice_form.is_valid():
            customer_id = request.POST.get('customer_id')
            customer = Customer.objects.get(id=customer_id)  # to check if customer is blocked
            if not customer.blocked_status:
                invoice_form.save()
            else:
                messages.error(request, 'We could not process your request user is being blocked.')
            return HttpResponseRedirect('/invoices')

    else:
        invoice_form = InvoiceForm()
        context = {'invoice_form': invoice_form}
        return render(request, 'create_invoice.html', context)




class GeneratePdf(View):
    def get(self, request, id, *args, **kwargs):
        invoice = Invoice.objects.get(id=id)
        total_price = 0
        for product in invoice.products.all():
            total_price += product.price
        data = {
            'id': invoice.id,
            'customer_name': invoice.customer_id.name,
            'products': invoice.products,
            'total_price': total_price,
            'shipping_address': invoice.shipping_address,
            'date': invoice.created_on
        }
        pdf = render_to_pdf('pdf/invoice_pdf.html', data)
        return HttpResponse(pdf, content_type='application/pdf')
