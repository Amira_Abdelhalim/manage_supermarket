# Supermarket Admin


 A simple website for supermarket admin to be able to view customers and create invoices.

### Prerequisits
- PostgreSQL
- Python3
- Django framework

### Installation
- Download the zip file or clone the repo
- create a database in postgresl <or your database>
- rename settings.py.example to settings.py change to your credentials
- run the following commands
"require python libraries"
```sh
$ pip3 install -r requirements.txt
```
"database migration"
```sh
$ python3 manage.py makemigrations
$ python3 manage.py migrate
```
"create superuser"
```sh
$ python3 manage.py createsuperuser
```
"run server"
```sh
$ python3 manage.py runserver
```
-Go to the url
```sh
http://127.0.0.1:8000/login
```


### Technologies
-Python3
-Django
-PostgreSQL
-HTML5
-CSS3
